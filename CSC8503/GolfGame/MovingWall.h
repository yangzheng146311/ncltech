#pragma once
#include "StateGameObject.h"
#include "../CSC8503Common/StateMachine.h"
#include "../CSC8503Common/State.h"
#include "../CSC8503Common/StateTransition.h"

namespace NCL {
	namespace CSC8503 {
		class MovingWall : public StateGameObject
		{
		public:
			MovingWall(GameObjectType t = GameObjectType::MovingWorldObject, string name = "");
			~MovingWall();

			virtual void OnCollisionBegin(GameObject* otherObject) {
				
			}

			virtual void OnCollisionEnd(GameObject* otherObject) {

			}

			virtual void updateState(float dt) {
				stateMachine->Update();
				Vector3 posFromStart = this->GetTransform().GetLocalPosition() - startingPosition;
				posFromStart.Normalise();
				Vector3 normalisedAxis = this->someData.axis.Normalised();
				float bias = 0.0001;
				dist = (startingPosition - this->GetTransform().GetLocalPosition()).Length();
				if (posFromStart.x > normalisedAxis.x - bias &&
					posFromStart.x < normalisedAxis.x + bias &&
					posFromStart.y > normalisedAxis.y - bias &&
					posFromStart.y < normalisedAxis.y + bias &&
					posFromStart.z > normalisedAxis.z - bias &&
					posFromStart.z < normalisedAxis.z + bias) {
					dist = -dist;
				}
			}

			virtual void initState();

			struct MovingWallStateData {
				PhysicsObject* physObject = nullptr;
				float speed = 0.0f;
				Vector3 axis = Vector3(1, 0, 0);
			};

			void setSpeed(float s) {
				someData.speed = s;
			}
			const float getSpeed() const {
				return someData.speed;
			}

			void setMovementAxis(Vector3 axis) {
				someData.axis = axis;
			}
			const Vector3 getMovementAxis() const {
				return someData.axis;
			}

			void setMaxDistance(float distance) {
				maxDistance = distance;
			}

			const float getMaxDistance() const {
				return maxDistance;
			}

		protected:
			StateFunc AFunc;
			StateFunc BFunc;
			GenericState* stateA;
			GenericState* stateB;
			GenericTransition<float&, float>* transitionA;
			GenericTransition<float&, float>* transitionB;
			Vector3 startingPosition;

			MovingWallStateData someData;

			float dist = 0.0f;
			float maxDistance = 200.0f;
		};
	}
}
