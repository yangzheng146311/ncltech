Controls on main menu:
	1: Start the game with no netowkring
	2: Start the game as a host (if a server already exists you'll join as a client)
	3: Start the game as a client

Controls in game:
	R: Reset the current level
	T: Reset the camera position (when not in ball camera)
	G: Toggle gravity
	B: Toggle ball camera
	I: Toggle drawing the A* path (only on robot level)
	O: Toggle drawing the navigation grid (only on robot levenl, lags in debug mode)
	P: Toggle drawing the quad tree
	Left: Go to the previous level
	Right: Go to the next level

To hit the ball: Click on the ball and then drag