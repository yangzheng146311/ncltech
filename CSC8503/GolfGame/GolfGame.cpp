#pragma once
#include "GolfGame.h"

#include "../CSC8503Common/GameWorld.h"
#include "../../Plugins/OpenGLRendering/OGLMesh.h"
#include "../../Plugins/OpenGLRendering/OGLShader.h"
#include "../../Plugins/OpenGLRendering/OGLTexture.h"
#include "../../Common/TextureLoader.h"

#include "../CSC8503Common/PositionConstraint.h"

using namespace NCL;
using namespace CSC8503;

//temp change collision contact points to local space
// change the impulse calculations to use use local space

GolfGame::GolfGame() {
	world = new GameWorld();
	renderer = new GolfGameRenderer(*world);
	physics = new PhysicsSystem(*world);

	forceMagnitude = 10.0f;
	useGravity = true;
	inSelectionMode = false;
	drawingBallShotVector = false;
	drawNavGrid = false;
	drawNavPath = false;
	currentLevel = 0;
	onMainMenu = true;
	camFollowBall = false;

	physics->UseGravity(useGravity);

	for (int i = 0; i < levelCount; ++i) {
		highScores.push_back(-1);
	}

	Debug::SetRenderer(renderer);

	InitialiseAssets();
}

GolfGame::~GolfGame() {
	delete cubeMesh;
	delete sphereMesh;
	delete basicTex;
	delete floorTex;
	delete ballTex;
	delete wallTex;
	delete robotTex;
	delete holeTex;
	delete basicShader;

	delete physics;
	delete renderer;
	delete world;
}

void GolfGame::InitialiseAssets() {
	cubeMesh = new OGLMesh("cube.msh");
	cubeMesh->SetPrimitiveType(GeometryPrimitive::Triangles);
	cubeMesh->UploadToGPU();

	sphereMesh = new OGLMesh("sphere.msh");
	sphereMesh->SetPrimitiveType(GeometryPrimitive::Triangles);
	sphereMesh->UploadToGPU();

	basicTex = (OGLTexture*)TextureLoader::LoadAPITexture("checkerboard.png");
	floorTex = (OGLTexture*)TextureLoader::LoadAPITexture("floor.png");
	ballTex = (OGLTexture*)TextureLoader::LoadAPITexture("ball.png");
	wallTex = (OGLTexture*)TextureLoader::LoadAPITexture("wall.png");
	robotTex = (OGLTexture*)TextureLoader::LoadAPITexture("robot.png");
	holeTex = (OGLTexture*)TextureLoader::LoadAPITexture("hole.png");
	basicShader = new OGLShader("GameTechVert.glsl", "GameTechFrag.glsl");

	InitCamera();
	InitMenuLevel();
}

void GolfGame::UpdateGame(float dt) {
	if (server) {
		server->UpdateServer();
		for (int i = 0; i < highScores.size(); ++i) {
			server->SendGlobalMessage(HighScorePacket(highScores.at(i), i));
		}
	}
	if (client)
		client->UpdateClient();

	if (onMainMenu) {
		drawMenu();
	}
	else {
		if (!inSelectionMode) {
			world->GetMainCamera()->UpdateCamera(dt);
		}

		UpdateKeys();

		if (useGravity) {
			Debug::Print("Gravity on", Vector2(10, 40));
		}
		else {
			Debug::Print("Gravity off", Vector2(10, 40));
		}

		int highScore = highScores.at(currentLevel);
		if (highScore < 0) {
			Debug::Print("High score for level " + std::to_string(currentLevel+1) + ": None!", Vector2(20, 650));
		}
		else {
			Debug::Print("High score for level " + std::to_string(currentLevel+1) + ": " + std::to_string(highScore), Vector2(20, 650));
		}

		if (golfBall) {
			Debug::Print(std::to_string(golfBall->getShotCount()) + string(" shots!"), Vector2(20, 630));
			if (golfBall->hasCollidedWithHole()) {
				if (client) {
					client->SendPacket(HighScorePacket(golfBall->getShotCount(), currentLevel));
				}
				else {
					if (highScores.at(currentLevel) == -1 || (golfBall->getShotCount() < highScores.at(currentLevel) && golfBall->getShotCount() > 0)) {
						highScores.at(currentLevel) = golfBall->getShotCount();
					}
				}
				currentLevel++;
				InitWorld(currentLevel);
			}
			if (golfBall->hasCollidedWithRobot()) {
				InitWorld(currentLevel);
				selectionObject = nullptr;
			}
		}

		if (robot && drawNavPath) {
			robot->drawPath();
		}
		if (currentLevelGrid && robot) {
			if (drawNavGrid) {
				currentLevelGrid->drawGrid();
			}
			Vector3 startPos = robot->GetTransform().GetLocalPosition();
			Vector3 endPos = golfBall->GetTransform().GetLocalPosition();
			NavigationPath path;
			if (currentLevelGrid->FindPath(startPos, endPos, path)) {
				robot->buildPath(path);
			}
			robot->moveAlongPath();
		}

		for (auto i : stateObjects) {
			i->updateState();
		}

		calculateShotVector();
	}

	if (camFollowBall) {
		moveCameraToBall();
	}

	world->UpdateWorld(dt);
	renderer->Update(dt);
	physics->Update(dt);

	Debug::FlushRenderables();
	renderer->Render();
}

void GolfGame::drawMenu() {
	if (Window::GetKeyboard()->KeyPressed(NCL::KeyboardKeys::KEYBOARD_1)) {
		onMainMenu = false;
		InitWorld(currentLevel);
	}
	if (Window::GetKeyboard()->KeyPressed(NCL::KeyboardKeys::KEYBOARD_2)) {
		InitNetworking();
		InitServer();
		InitClient();
		onMainMenu = false;
		InitWorld(currentLevel);
	}
	if (Window::GetKeyboard()->KeyPressed(NCL::KeyboardKeys::KEYBOARD_3)) {
		InitNetworking();
		InitClient();
		onMainMenu = false;
		InitWorld(currentLevel);
	}

	Debug::Print("Press 1 to play solo", Vector2(25, 360 - (25)));
	Debug::Print("Press 2 to host a game", Vector2(25, 360 - (25*2)));
	Debug::Print("Press 3 to connect to a game", Vector2(25, 360 - (25*3)));
}

void GolfGame::UpdateKeys() {
	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_R)) {
		InitWorld(currentLevel);
		selectionObject = nullptr;
	}

	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_T)) {
		resetCameraPosition();
	}

	if (Window::GetKeyboard()->KeyPressed(NCL::KeyboardKeys::KEYBOARD_G)) {
		useGravity = !useGravity; //Toggle gravity!
		physics->UseGravity(useGravity);
	}

	if (Window::GetKeyboard()->KeyPressed(NCL::KeyboardKeys::KEYBOARD_B)) {
		toggleCamFollowBall();
	}

	//Running certain physics updates in a consistent order might cause some
	//bias in the calculations - the same objects might keep 'winning' the constraint
	//allowing the other one to stretch too much etc. Shuffling the order so that it
	//is random every frame can help reduce such bias.
	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_F9)) {
		world->ShuffleConstraints(true);
	}
	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_F10)) {
		world->ShuffleConstraints(false);
	}

	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_F7)) {
		world->ShuffleObjects(true);
	}
	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_F8)) {
		world->ShuffleObjects(false);
	}

	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_P)) {
		physics->setDrawQuadTree(!physics->getDrawQuadTree());
	}

	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_O)) {
		drawNavGrid = !drawNavGrid;
	}

	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_I)) {
		drawNavPath = !drawNavPath;
	}

	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_RIGHT)) {
		currentLevel++;
		InitWorld(currentLevel);
	}
	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_LEFT)) {
		currentLevel--;
		InitWorld(currentLevel);
	}
}

void GolfGame::InitCamera() {
	world->GetMainCamera()->SetNearPlane(3.0f);
	world->GetMainCamera()->SetFarPlane(20000.0f);
	world->GetMainCamera()->SetPitch(-40.0f);
	world->GetMainCamera()->SetYaw(0.0f);
	world->GetMainCamera()->SetPosition(defaultCameraPos);
}

void GolfGame::resetCameraPosition() {
	world->GetMainCamera()->SetPitch(-40.0f);
	world->GetMainCamera()->SetYaw(0.0f);
	world->GetMainCamera()->SetPosition(defaultCameraPos);
}

void GolfGame::moveCameraToBall() {
	world->GetMainCamera()->SetPosition(golfBall->GetTransform().GetLocalPosition() + Vector3(0, 300, 500));
	world->GetMainCamera()->SetPitch(-30.0f);
	world->GetMainCamera()->SetYaw(0.0f);
	Debug::Print("Ball Cam!", Vector2(1000, 650));
}

void GolfGame::InitWorld(int i) {
	world->ClearAndErase();
	physics->Clear();
	stateObjects.clear();

	selectionObject = nullptr;
	golfBall = nullptr;
	movingWall = nullptr;
	robot = nullptr;

	delete currentLevelGrid;
	currentLevelGrid = nullptr;

	if (i < 0)
		i = levelCount - 1;
	if (i >= levelCount)
		i = i % levelCount;

	currentLevel = i;

	switch (i) {
	case 0: InitLevelOne(); break;
	case 1: InitLevelTwo(); break;
	case 2: InitLevelThree(); break;
	case 3: InitLevelFour(); break;
	case 4: InitLevelFive(); break;
	}
}

GameObject* GolfGame::AddFloorToWorld(const Vector3& position, string name) {
	GameObject* floor = new GameObject(GameObjectType::StaticWorldObject, name);
	Vector3 floorSize = Vector3(5000, 10, 5000);
	AABBVolume* volume = new AABBVolume(floorSize);

	floor->SetBoundingVolume((CollisionVolume*)volume);
	floor->GetTransform().SetWorldScale(floorSize);
	floor->GetTransform().SetWorldPosition(position);

	floor->SetRenderObject(new RenderObject(&floor->GetTransform(), cubeMesh, floorTex, basicShader));
	floor->SetPhysicsObject(new PhysicsObject(&floor->GetTransform(), floor->GetBoundingVolume()));
	floor->GetPhysicsObject()->SetInverseMass(0);
	floor->GetPhysicsObject()->InitCubeInertia();

	world->AddGameObject(floor);

	return floor;
}

GameObject* GolfGame::AddWallToWorld(const Vector3& position, Vector3 dimensions, string name) {
	GameObject* wall = new GameObject(GameObjectType::StaticWorldObject, name);
	Vector3 wallSize = Vector3(dimensions);
	AABBVolume* volume = new AABBVolume(wallSize);

	wall->SetBoundingVolume((CollisionVolume*)volume);
	wall->GetTransform().SetWorldScale(wallSize);
	wall->GetTransform().SetWorldPosition(position);

	wall->SetRenderObject(new RenderObject(&wall->GetTransform(), cubeMesh, wallTex, basicShader));
	wall->SetPhysicsObject(new PhysicsObject(&wall->GetTransform(), wall->GetBoundingVolume()));
	wall->GetPhysicsObject()->SetInverseMass(0);
	wall->GetPhysicsObject()->InitCubeInertia();

	world->AddGameObject(wall);

	return wall;
}

GameObject* GolfGame::AddSphereToWorld(const Vector3& position, float radius, float inverseMass, string name) {
	GameObject* sphere = new GameObject(GameObjectType::GameObject, name);
	Vector3 sphereSize = Vector3(radius, radius, radius);
	SphereVolume* volume = new SphereVolume(radius);

	sphere->SetBoundingVolume((CollisionVolume*)volume);
	sphere->GetTransform().SetWorldScale(sphereSize);
	sphere->GetTransform().SetWorldPosition(position);

	sphere->SetRenderObject(new RenderObject(&sphere->GetTransform(), sphereMesh, wallTex, basicShader));
	sphere->SetPhysicsObject(new PhysicsObject(&sphere->GetTransform(), sphere->GetBoundingVolume()));
	sphere->GetPhysicsObject()->SetInverseMass(inverseMass);
	sphere->GetPhysicsObject()->InitSphereInertia();

	world->AddGameObject(sphere);

	return sphere;
}

GameObject* GolfGame::AddCubeToWorld(const Vector3& position, Vector3 dimensions, float inverseMass, string name) {
	GameObject* cube = new GameObject(GameObjectType::GameObject, name);
	AABBVolume* volume = new AABBVolume(dimensions);

	cube->SetBoundingVolume((CollisionVolume*)volume);
	cube->GetTransform().SetWorldPosition(position);
	cube->GetTransform().SetWorldScale(dimensions);

	cube->SetRenderObject(new RenderObject(&cube->GetTransform(), cubeMesh, wallTex, basicShader));
	cube->SetPhysicsObject(new PhysicsObject(&cube->GetTransform(), cube->GetBoundingVolume()));
	cube->GetPhysicsObject()->SetInverseMass(inverseMass);
	cube->GetPhysicsObject()->InitCubeInertia();

	world->AddGameObject(cube);

	return cube;
}

GameObject* GolfGame::AddCubeOBBToWorld(const Vector3& position, Vector3 dimensions, float inverseMass, string name) {
	GameObject* cube = new GameObject(GameObjectType::GameObject, name);
	OBBVolume* volume = new OBBVolume(dimensions);

	cube->SetBoundingVolume((CollisionVolume*)volume);
	cube->GetTransform().SetWorldPosition(position);
	cube->GetTransform().SetWorldScale(dimensions);

	cube->SetRenderObject(new RenderObject(&cube->GetTransform(), cubeMesh, wallTex, basicShader));
	cube->SetPhysicsObject(new PhysicsObject(&cube->GetTransform(), cube->GetBoundingVolume()));
	cube->GetPhysicsObject()->SetInverseMass(inverseMass);
	cube->GetPhysicsObject()->InitCubeInertia();

	world->AddGameObject(cube);

	return cube;
}

GameObject* GolfGame::AddGolfBallToWorld(const Vector3& position, float radius, float inverseMass, string name) {
	GolfBall* ball = new GolfBall(GameObjectType::GameObject, name);
	SphereVolume* volume = new SphereVolume(radius);
	Vector3 ballSize = Vector3(radius, radius, radius);

	ball->SetBoundingVolume((CollisionVolume*)volume);
	ball->GetTransform().SetWorldScale(ballSize);
	ball->GetTransform().SetWorldPosition(position);

	ball->SetRenderObject(new RenderObject(&ball->GetTransform(), sphereMesh, ballTex, basicShader));
	ball->SetPhysicsObject(new PhysicsObject(&ball->GetTransform(), ball->GetBoundingVolume()));
	ball->GetPhysicsObject()->SetInverseMass(inverseMass);
	ball->GetPhysicsObject()->InitSphereInertia();

	world->AddGameObject(ball);

	return ball;
}

GameObject* GolfGame::AddGolfHoleToWorld(const Vector3& position, Vector3 dimensions, string name) {
	GameObject* hole = new GameObject(GameObjectType::GhostObject, name);
	AABBVolume* volume = new AABBVolume(dimensions);

	hole->SetBoundingVolume((CollisionVolume*)volume);
	hole->GetTransform().SetWorldScale(dimensions);
	hole->GetTransform().SetWorldPosition(position);

	hole->SetRenderObject(new RenderObject(&hole->GetTransform(), cubeMesh, holeTex, basicShader));
	hole->SetPhysicsObject(new PhysicsObject(&hole->GetTransform(), hole->GetBoundingVolume()));
	hole->GetPhysicsObject()->SetInverseMass(0.0f);
	hole->GetPhysicsObject()->InitCubeInertia();

	world->AddGameObject(hole);

	return hole;
}

GameObject* GolfGame::AddMovingWallToWorld(const Vector3& position, Vector3 dimensions, float maxDistance, float speed, Vector3 movementAxis, string name) {
	MovingWall* movingWall = new MovingWall(GameObjectType::MovingWorldObject, name);
	AABBVolume* volume = new AABBVolume(dimensions);

	movingWall->SetBoundingVolume((CollisionVolume*)volume);
	movingWall->GetTransform().SetWorldScale(dimensions);
	movingWall->GetTransform().SetWorldPosition(position);

	movingWall->SetRenderObject(new RenderObject(&movingWall->GetTransform(), cubeMesh, wallTex, basicShader));
	movingWall->SetPhysicsObject(new PhysicsObject(&movingWall->GetTransform(), movingWall->GetBoundingVolume()));
	movingWall->GetPhysicsObject()->SetInverseMass(0.0f);
	movingWall->GetPhysicsObject()->InitCubeInertia();

	movingWall->setSpeed(speed);
	movingWall->setMovementAxis(movementAxis);
	movingWall->setMaxDistance(maxDistance);
	movingWall->initState();

	world->AddGameObject(movingWall);
	stateObjects.push_back(movingWall);

	return movingWall;
}

GameObject* GolfGame::AddBallChasingRobotToWorld(const Vector3& position, Vector3 dimensions, float inverseMass, string name) {
	BallChasingRobot* robot = new BallChasingRobot(GameObjectType::GameObject, name);
	AABBVolume* volume = new AABBVolume(dimensions);

	robot->SetBoundingVolume((CollisionVolume*)volume);
	robot->GetTransform().SetWorldScale(dimensions);
	robot->GetTransform().SetWorldPosition(position);

	robot->SetRenderObject(new RenderObject(&robot->GetTransform(), cubeMesh, robotTex, basicShader));
	robot->SetPhysicsObject(new PhysicsObject(&robot->GetTransform(), robot->GetBoundingVolume()));
	robot->GetPhysicsObject()->SetInverseMass(inverseMass);
	robot->GetPhysicsObject()->InitCubeInertia();

	world->AddGameObject(robot);
	stateObjects.push_back(robot);

	return robot;
}

GameObject* GolfGame::AddOBBWallToWorld(const Vector3& position, Vector3 dimensions, Quaternion rotation, string name) {
	GameObject* wall = new GameObject(GameObjectType::StaticWorldObject, name);
	Vector3 wallSize = Vector3(dimensions);
	OBBVolume* volume = new OBBVolume(wallSize);

	wall->SetBoundingVolume((CollisionVolume*)volume);
	wall->GetTransform().SetWorldScale(wallSize);
	wall->GetTransform().SetWorldPosition(position);
	wall->GetTransform().SetLocalOrientation(rotation);

	wall->SetRenderObject(new RenderObject(&wall->GetTransform(), cubeMesh, wallTex, basicShader));
	wall->SetPhysicsObject(new PhysicsObject(&wall->GetTransform(), wall->GetBoundingVolume()));
	wall->GetPhysicsObject()->SetInverseMass(0);
	wall->GetPhysicsObject()->InitCubeInertia();

	world->AddGameObject(wall);

	return wall;
}

void GolfGame::InitMenuLevel() {
	AddFloorToWorld(Vector3(10, -100, 1), "Floor");
}

void GolfGame::InitTestLevel() {
	//AddSphereToWorld(Vector3(5, 5, 5), 10, 10.0f, "Sphere 1");
	//AddSphereToWorld(Vector3(5, 5, -35), 10, 10.0f, "Sphere 2");
	//AddCubeToWorld(Vector3(5, 5, 75), Vector3(10, 10, 10), 10.0f, "Cube 2");
	AddCubeToWorld(Vector3(25, 5, 60), Vector3(30, 30, 30), 10.0f, "Cube 1");
	AddWallToWorld(Vector3(20, -70, 20), Vector3(50,50,50), "Wall");
	//AddFloorToWorld(Vector3(10, -100, 1), "Floor");
}

void GolfGame::InitLevelOne() {
	AddFloorToWorld(Vector3(10, -100, 1), "Floor");
	AddWallToWorld(Vector3(-500, -80, 0), Vector3(50,100,510), "Wall");
	AddWallToWorld(Vector3(500, -80, 0), Vector3(50,100,510), "Wall");
	AddWallToWorld(Vector3(0, -80, -500), Vector3(490,100,50), "Wall");
	AddWallToWorld(Vector3(0, -80, 500), Vector3(490,100,50), "Wall");
	golfBall = (GolfBall*)AddGolfBallToWorld(Vector3(0, -50, 150), 40.0f, 10.0f, "Golf Ball");
	AddGolfHoleToWorld(Vector3(0, -95, -300), Vector3(50, 10, 50), "Hole");
}

void GolfGame::InitLevelTwo() {
	AddFloorToWorld(Vector3(10, -100, 1), "Floor");
	AddWallToWorld(Vector3(-500, -80, 0), Vector3(50,100,510), "Wall");
	AddWallToWorld(Vector3(500, -80, 0), Vector3(50,100,510), "Wall");
	AddWallToWorld(Vector3(0, -80, -500), Vector3(490,100,50), "Wall");
	AddWallToWorld(Vector3(0, -80, 500), Vector3(490,100,50), "Wall");
	movingWall = (MovingWall*)AddMovingWallToWorld(Vector3(0, -60, -150), Vector3(150, 30, 40), 200.0f, 200.0f, Vector3(1,0,0), "Moving Wall");
	golfBall = (GolfBall*)AddGolfBallToWorld(Vector3(0, -50, 150), 40.0f, 10.0f, "Golf Ball");
	AddGolfHoleToWorld(Vector3(0, -95, -300), Vector3(50, 10, 50), "Hole");
}

void GolfGame::InitLevelThree() {
	AddFloorToWorld(Vector3(10, -100, 1), "Floor");
	AddWallToWorld(Vector3(-500, -80, 0), Vector3(50,100,510), "Wall");
	AddWallToWorld(Vector3(500, -80, 0), Vector3(50,100,510), "Wall");
	AddWallToWorld(Vector3(0, -80, -500), Vector3(490,100,50), "Wall");
	AddWallToWorld(Vector3(0, -80, 500), Vector3(490,100,50), "Wall");

	AddWallToWorld(Vector3(0, -80, 0), Vector3(25,100,210), "Wall");
	AddWallToWorld(Vector3(105, -80, 0), Vector3(105,100,25), "Wall");

	world->UpdateWorld(1);

	currentLevelGrid = new NavigationGrid(20, 80, 80, Vector3(-800, 0, -800), *world);
	
	golfBall = (GolfBall*)AddGolfBallToWorld(Vector3(300, -50, 0), 40.0f, 10.0f, "Golf Ball");
	robot = (BallChasingRobot*)AddBallChasingRobotToWorld(Vector3(-150, -70, 0), Vector3(10, 10, 10), 10.0f, "Robot");
	robot->initState();
	robot->setTarget(golfBall);
	robot->setWorld(world);
	AddGolfHoleToWorld(Vector3(-250, -95, -20), Vector3(50, 10, 50), "Hole");
}

void GolfGame::InitLevelFour() {
	AddFloorToWorld(Vector3(10, -100, 1), "Floor");
	AddWallToWorld(Vector3(-500, -80, 0), Vector3(50, 200, 510), "Wall");
	AddWallToWorld(Vector3(500, -80, 0), Vector3(50, 200, 510), "Wall");
	AddWallToWorld(Vector3(0, -80, -500), Vector3(490, 200, 50), "Wall");
	AddWallToWorld(Vector3(0, -80, 500), Vector3(490, 200, 50), "Wall");

	
	AddOBBWallToWorld(Vector3(0, -100, 100), Vector3(50, 30, 100), Quaternion(0.1, 0, 0, 1), "Wall");
	AddOBBWallToWorld(Vector3(-50, 0, -300), Vector3(100, 200, 50), Quaternion(0, 0.5, 0, 1), "Wall");

	AddWallToWorld(Vector3(300, -80, -300), Vector3(80, 50, 80), "Wall");
	AddGolfHoleToWorld(Vector3(300, -30, -300), Vector3(50, 10, 50), "Hole");

	world->UpdateWorld(1);

	golfBall = (GolfBall*)AddGolfBallToWorld(Vector3(0, -50, 220), 40.0f, 10.0f, "Golf Ball");

}

void GolfGame::InitLevelFive() {
	AddFloorToWorld(Vector3(10, -100, 1), "Floor");
	AddWallToWorld(Vector3(-500, -80, 0), Vector3(50, 100, 510), "Wall");
	AddWallToWorld(Vector3(500, -80, 0), Vector3(50, 100, 510), "Wall");
	AddWallToWorld(Vector3(0, -80, -500), Vector3(490, 100, 50), "Wall");
	AddWallToWorld(Vector3(0, -80, 500), Vector3(490, 100, 50), "Wall");

	float sizeMultiplier = 1.2f;

	Vector3 cubeSize = Vector3(8, 8, 8) * sizeMultiplier;
	int numLinks = 32;
	Vector3 offset(-400, 100, -100);
	MovingWall* start = (MovingWall*)AddMovingWallToWorld(offset, cubeSize, 200.0f, 100.0f, Vector3(0, 0, 1));
	MovingWall* end = (MovingWall*)AddMovingWallToWorld(Vector3((numLinks + 2) * 20 * sizeMultiplier, 0, 0) + offset, cubeSize, 200.0f, 100.0f, Vector3(0,0,1));
	movingWall = end;

	GameObject* previous = start;

	for (int i = 0; i < numLinks; ++i) {
		GameObject* block = AddCubeToWorld(Vector3((i + 1) * 20 * sizeMultiplier, 0, 0) + offset, cubeSize, 10.0f);
		PositionConstraint* constraint = new PositionConstraint(previous, block, 30.0f);
		world->AddConstraint(constraint);
		previous = block;
	}

	PositionConstraint* constraint = new PositionConstraint(previous, end, 30.0f);
	world->AddConstraint(constraint);

	golfBall = (GolfBall*)AddGolfBallToWorld(Vector3(0, -50, 250), 40.0f, 10.0f, "Golf Ball");
	AddGolfHoleToWorld(Vector3(0, -95, -300), Vector3(50, 10, 50), "Hole");
}

void GolfGame::calculateShotVector() {
	if (Window::GetKeyboard()->KeyPressed(KEYBOARD_Q)) {
		inSelectionMode = !inSelectionMode;
		if (inSelectionMode) {
			Window::GetWindow()->ShowOSPointer(true);
			Window::GetWindow()->LockMouseToWindow(false);
		}
		else {
			Window::GetWindow()->ShowOSPointer(false);
			Window::GetWindow()->LockMouseToWindow(true);
		}
	}
	if (inSelectionMode) {
		Debug::Print("Press Q to change to camera mode", Vector2(Vector2(10, 18)));
		if (Window::GetMouse()->ButtonDown(NCL::MouseButtons::MOUSE_LEFT)) {
			if (!drawingBallShotVector) {

				Ray ray = CollisionDetection::BuildRayFromMouse(*world->GetMainCamera());
				RayCollision closestCollision;
				if (world->Raycast(ray, closestCollision, true, GameObjectType::GameObject)) {
					if (closestCollision.node == golfBall) {
						drawingBallShotVector = true;
					}
				}
			}
			else {
				Ray ray = CollisionDetection::BuildRayFromMouse(*world->GetMainCamera());
				Vector3 ballPos = golfBall->GetTransform().GetWorldPosition();
				float yDiff = ray.GetPosition().y - ballPos.y;

				float multi = yDiff / ray.GetDirection().y;

				Vector3 mousePosAtBallHeight = ray.GetPosition() - (ray.GetDirection() * multi);

				ballShotVector = ballPos - mousePosAtBallHeight;
				if (ballShotVector.Length() > golfBall->getMaxShotForce()) {
					ballShotVector.Normalise();
					ballShotVector = ballShotVector * golfBall->getMaxShotForce();
				}

				Debug::DrawLine(ballPos, ballPos - ballShotVector);
				
			}
		}
		else {
			if (drawingBallShotVector) {
				golfBall->GetPhysicsObject()->AddForce(ballShotVector * 100.0f);
				drawingBallShotVector = false;
				if (!golfBall->hasCollidedWithHole())
					golfBall->incrementShotCount();
			}
		}
	}
	else {
		Debug::Print("Press Q to change to select mode", Vector2(Vector2(10, 18)));
	}
}

void GolfGame::InitNetworking() {
	std::cout << "Initializing networking..." << std::endl;
	NetworkBase::Initialise();
}

bool GolfGame::InitServer() {
	std::cout << "Creating server" << std::endl;
	serverReceiver = new HighScoreReceiver("Server", &highScores);
	int port = NetworkBase::GetDefaultPort();
	server = new GameServer(port, 20);
	if (server->hasInitialised()) {
		server->RegisterPacketHandler(BasicNetworkMessages::HighScore_Message, serverReceiver);
		std::cout << "Server created" << std::endl;
	}
	else {
		std::cout << "Server creation failed." << std::endl;
		delete server;
		server = nullptr;
		return false;
	}
	return true;
}

void GolfGame::InitClient() {
	std::cout << "Creating client" << std::endl;
	clientReceiver = new HighScoreReceiver("Client", &highScores);
	int port = NetworkBase::GetDefaultPort();
	client = new GameClient();
	client->RegisterPacketHandler(BasicNetworkMessages::HighScore_Message, clientReceiver);
	bool canConnect = client->Connect(127, 0, 0, 1, port);
	if (!canConnect) {
		client->Destroy();
		delete client;
		client = nullptr;
	}
	else {
		std::cout << "Connected to server" << std::endl;
	}
}